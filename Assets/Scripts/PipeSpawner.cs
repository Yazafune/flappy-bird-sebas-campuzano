using System.Collections;
using UnityEngine;
using UnityEngine.Pool;

namespace FlappyBird
{
    public class PipeSpawner : MonoBehaviour
    {
        [SerializeField] private Transform spawnPoint;
        [SerializeField] private PipeDay pipeDay;
        [SerializeField] private PipeNight pipeNight;
        private ObjectPool<PipeDay> pipesDayPool;
        private ObjectPool<PipeNight> pipesNightPool;

        [Space, SerializeField, Range(-1, 1)] private float minHeight;
        [SerializeField, Range(-1, 1)] private float maxHeight;

        [Space, SerializeField] private float timeToSpawnFirstPipe;
        [SerializeField] private float timeToSpawnPipe;

        int time, player;
        public GameObject fondoDia, fondoNoche;
        public GameObject birdBlue, birdYellow, birdRed;

        void Start()
        {
            player = Random.Range(1, 4);
            time = Random.Range(1, 3);
            Debug.Log("player: " + player + " time: " + time);
            switch (player)
            {
                case 1:
                    Instantiate(birdRed);
                    break;
                case 2:
                    Instantiate(birdBlue);
                    break;
                case 3:
                    Instantiate(birdYellow);
                    break;
            }
            if (time == 1)
            {
                fondoDia.SetActive(true);
                pipesDayPool = new ObjectPool<PipeDay>(() =>
                {
                    PipeDay pipe = Instantiate(pipeDay, GetSpawnPosition(), Quaternion.identity);
                    pipe.DesactivarPipe(DesactivarPipePoolDay);
                    return pipe;
                }, pipe =>
                {
                    pipe.transform.position = GetSpawnPosition();
                    pipe.gameObject.SetActive(true);
                }, pipe =>
                {
                    pipe.gameObject.SetActive(false);
                }, pipe =>
                {
                    Destroy(pipe.gameObject);
                }, true, 2, 3);
                StartCoroutine(SpawnDayPipes());
            }
            else
            {
                fondoNoche.SetActive(true);
                pipesNightPool = new ObjectPool<PipeNight>(() =>
                {
                    PipeNight pipe = Instantiate(pipeNight, GetSpawnPosition(), Quaternion.identity);
                    pipe.DesactivarPipe(DesactivarPipePoolNight);
                    return pipe;
                }, pipe =>
                {
                    pipe.transform.position = GetSpawnPosition();
                    pipe.gameObject.SetActive(true);
                }, pipe =>
                {
                    pipe.gameObject.SetActive(false);
                }, pipe =>
                {
                    Destroy(pipe.gameObject);
                }, true, 2, 3);
                StartCoroutine(SpawnNightPipes());
            }


        }

        private Vector3 GetSpawnPosition()
        {
            return new Vector3(spawnPoint.position.x, Random.Range(minHeight, maxHeight), spawnPoint.position.z);
        }

        private IEnumerator SpawnDayPipes()
        {
            yield return new WaitForSeconds(timeToSpawnFirstPipe);

            pipesDayPool.Get();

            do
            {
                yield return new WaitForSeconds(timeToSpawnPipe);

                pipesDayPool.Get();
            } while (true);
        }
        private IEnumerator SpawnNightPipes()
        {
            yield return new WaitForSeconds(timeToSpawnFirstPipe);

            pipesNightPool.Get();

            do
            {
                yield return new WaitForSeconds(timeToSpawnPipe);

                pipesNightPool.Get();
            } while (true);
        }
        private void DesactivarPipePoolDay(PipeDay pipeD)
        {
            pipesDayPool.Release(pipeD);
        }
        private void DesactivarPipePoolNight(PipeNight pipeN)
        {
            pipesNightPool.Release(pipeN);
        }

        public void Stop()
        {
            StopAllCoroutines();
        }
    }
}