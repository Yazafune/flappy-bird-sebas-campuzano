using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDayNightSelector : MonoBehaviour
{
    int time, player;
    public GameObject fondoDia, fondoNoche;
    public GameObject birdBlue, birdYellow, birdRed;
    // Start is called before the first frame update
    void Start()
    {
        player = Random.Range(1, 4);
        time = Random.Range(1, 3);
        Debug.Log("player: " + player + " time: " + time);

        switch (player)
        {
            case 1:
                Instantiate(birdRed);
                break;
            case 2:
                Instantiate(birdBlue);
                break;
            case 3:
                Instantiate(birdYellow);
                break;
        }

        if (time == 1)
        {
            fondoDia.SetActive(true);
        }
        else
        {
            fondoNoche.SetActive(true);
        }
    }

}
