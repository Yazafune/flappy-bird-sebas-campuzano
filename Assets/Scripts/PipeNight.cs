using System;
using System.Collections;
using UnityEngine;

namespace FlappyBird
{
    public class PipeNight : MonoBehaviour
    {
        [SerializeField]
        private float speed;
        [SerializeField]
        private float timeToDestroyPipe;
        private Action<PipeNight> desactivarAccion;

        private void OnEnable()
        {
            StartCoroutine(DesactivarPipeTiempo());
        }

        private void Update()
        {
            if (GameManager.Instance.isGameOver) return;
            transform.position += (Vector3.left * Time.deltaTime * speed);
        }

        private IEnumerator DesactivarPipeTiempo()
        {
            yield return new WaitForSeconds(timeToDestroyPipe);
            desactivarAccion(this);
        }
        public void DesactivarPipe(Action<PipeNight> desactivarAccionParametro)
        {
            desactivarAccion = desactivarAccionParametro;
        }
    }
}