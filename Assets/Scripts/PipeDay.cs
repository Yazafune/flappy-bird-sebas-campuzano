using System.Collections;
using UnityEngine;
using System;

namespace FlappyBird
{
    public class PipeDay : MonoBehaviour
    {
        [SerializeField]
        private float speed;
        [SerializeField]
        private float timeToDestroyPipe;
        private Action<PipeDay> desactivarAccion;

        private void OnEnable()
        {
            StartCoroutine(DesactivarPipeTiempo());
        }

        private void Update()
        {
            if (GameManager.Instance.isGameOver) return;
            transform.position += (Vector3.left * Time.deltaTime * speed);
        }

        private IEnumerator DesactivarPipeTiempo()
        {
            yield return new WaitForSeconds(timeToDestroyPipe);
            desactivarAccion(this);
        }
        public void DesactivarPipe(Action<PipeDay> desactivarAccionParametro)
        {
            desactivarAccion = desactivarAccionParametro;
        }
    }
}